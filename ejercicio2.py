# Escribe otro programa que pida una lista de números como
# la anterior y al ﬁnal muestre por pantalla el máximo y mínimo de los
# números, en vez de la media.

i=0
t=0
lista=[]

while True:
    num = input('Ingrese un número o fin para terminar: ')
    lista.append(num)
    if num.lower() in 'fin':
        break
    try:
        t= t+int(num)
        i=i+1
        mayor=max(lista)
        menor=min(lista)
    except ValueError:
        print('Entrada invalida')
print('Proceso terminado')

print('La suma es: ',t)
print('La cantidad de numeros introducidos es: ', i)
print('El numero mayor es: ', mayor)
print('El numero menor es: ', menor)