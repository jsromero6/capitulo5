# Escribe un programa que lea repetidamente números hasta
# que el usuario introduzca “ﬁn”. Una vez se haya introducido “ﬁn”,
# muestra por pantalla el total, la cantidad de números y la media de
# esos números. Si el usuario introduce cualquier otra cosa que no sea un
# número, detecta su fallo usando try y except, muestra un mensaje de
# error y pasa al número siguiente.

i=0
t=0
while True:
    num = input('Ingrese un número o fin para terminar: ')
    if num.lower() in 'fin':
        break
    try:
        t= t+int(num)
        i=i+1
    except ValueError:
        print('Entrada invalida')
print('Proceso terminado')

print('La suma es: ',t)
print('La cantidad de numeros introducidos es: ', i)
print('El promedio de los números ingresados es: ', t/i)